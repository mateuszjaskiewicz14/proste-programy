package sdacademy.d2;

public class Circle extends Shape {
    private double beam;

    public Circle(String color, double beam) {
        super(color);
        this.beam = beam;
    }
    public double area(){
        return Math.PI*(beam*beam);
    }
    public double perimeter(){

        return 2*Math.PI*beam;
    }


}
