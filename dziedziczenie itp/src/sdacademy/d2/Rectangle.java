package sdacademy.d2;

public class Rectangle extends Shape {
    private double side;
    private double height;

    public Rectangle(String color,double side,double height){
        super(color);
        this.side = side;
        this.height = height;

    }
    public double area (){
        return side*height;

    }
    public double perimeter(){
        return 2*side + 2*height;
    }
}
