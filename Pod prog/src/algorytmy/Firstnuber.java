package algorytmy;


import java.util.Arrays;
import java.util.Scanner;

public class Firstnuber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        firstNumber(scanner.nextInt());
    }

    static boolean[] firstNumber(int tablenght) {
        boolean[] primeArray = new boolean[tablenght];
        Arrays.fill(primeArray, true);

        primeArray[0] = false;
        primeArray[1] = false;
        for (int i = 0; i < primeArray.length; i++) {
            if (primeArray[i]) {
                for (int j = i * i; j < primeArray.length; j += i) {
                    primeArray[j] = false;
                }
            }
        }
        return primeArray;

    }
}
