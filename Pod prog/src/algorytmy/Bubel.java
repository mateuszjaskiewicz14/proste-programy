package algorytmy;


import java.util.Random;

public class Bubel {
    public static void main(String[] args) {

        int a = 5;
        int b = 12;
        int[] transfer = randoTab(a, b);
        printArray(transfer);
        printArray(sortTab(transfer));


    }

    static int[] randoTab(int a, int b) {
        Random random = new Random();
        int[] randomTable = new int[a];
        for (int i = 0; i < a; i++) {
            randomTable[i] = random.nextInt(b);
        }
        return randomTable;
    }

    static int[] sortTab(int[] array) {

        for (int i = 0; i < array.length - 1; i++) {
            int n = 0;
            for (int j = array.length - 1; j > 0; j--) {

                if (array[n] > array[n + 1]) {
                    int y = array[n];
                    array[n] = array[n + 1];
                    array[n + 1] = y;
                }
                n++;
            }
        }
        return array;
    }

    static void printArray(int[] viueTab) {
        for (int print : viueTab)
            System.out.println(print);
        System.out.println();
    }
}
