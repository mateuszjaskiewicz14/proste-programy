package rekurencyjny.ciągfibonacciego;

public class sequence {
    public static void main(String[] args){
        System.out.println(sequence(8));

    }
    public static int sequence(int n){
        int sfn = 1;
        if(n==0){
            sfn =0;
        }else if(n==1){
            sfn = 1;
        }else if(n==2){
            sfn = 1;
        }else {
            for (int i = 0; i < n; i++) {
                sfn = sequence(n - 1) + sequence(n - 2);

            }
        }
        return sfn;
    }

}
