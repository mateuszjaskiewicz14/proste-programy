package pl.com.sda.homework;

public class MainProgram {
    public static void main(String[] args){

        System.out.println(zeroPlace(1,4,0));

    }public static int delta(int a, int b, int c){
        return (b*b)-4*a*c;
    }
    public static String zeroPlace(int a, int b, int c){

        if( delta(a,b,c)>0){
           double x2 = ZeroPlace.deltax2(a,b,delta(a, b, c));
           double x1 = ZeroPlace.deltax1(a,b,delta(a, b, c));
           return "x1 = "+x1+"\n"+"x2 = "+ x2;
        }else if(delta(a,b,c)==0) {
            double x0 = ZeroPlace.deltax0(a, b);
            return "x0 = "+x0;
        }
            return "Brak miejsca zerowego";
    }
}
