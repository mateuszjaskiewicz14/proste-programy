package stream;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Workers<employeeL> {
    public static void main(String[] args) {

        List<Employee> employee = Arrays.asList(
                new Employee("Kowal", "Jan", 34, 3400.0),
                new Employee("As", "Ala", 27, 4100.0),
                new Employee("Kot", "Zofia", 33, 3700.0),
                new Employee("Puchacz", "Jan", 41, 3600.0)
        );
        List<Employee> employeeL = employee
                .stream()
                .filter(q -> q.getAge() > 30 && q.getSalary() < 4000)
                .collect(Collectors.toList());

        for (Employee emp : employeeL) {
            System.out.println(emp.toString());
        }
    }


}
