package stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Strem {
    public static void main(String[] args) {
        List<Integer> source = Arrays.asList(5,72,8,11,9);
        List<Integer> target = source
                .stream()
         .filter(q->q<10)
                .map(q->q*q)
                .collect(Collectors.toList());
        for (Integer integer:target){
            System.out.println(integer);
        }
    }
}
