package stream;

public class Employee {
    private final String name;
    private final String surname;
    private final int age;
    private final double salary;

    public Employee(String name, String surname, int Age, double salary){


        this.name = name;
        this.surname = surname;
        age = Age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
