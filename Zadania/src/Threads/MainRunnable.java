package Threads;

public class MainRunnable {
    public static void main(String[] args) {
        MyRunnable runnable = new MyRunnable();
        Thread thread = new Thread(runnable);

    }
    public static class MyRunnable implements Runnable{

        @Override
        public void run() {
            System.out.println("Hello " + Thread.currentThread().getName());
        }
    }
}
