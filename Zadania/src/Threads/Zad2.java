package Threads;

import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {
        TreadMatch runnable1 = new TreadMatch();
        ScannTread scannTread = new ScannTread(runnable1);
        Thread loopMath = new Thread(runnable1);
        Thread scannObserw = new Thread(scannTread);
        loopMath.start();
        scannObserw.start();
    }

    public static class ScannTread implements Runnable {
        private final TreadMatch mathTread;

        public ScannTread(TreadMatch mathTread) {
            this.mathTread = mathTread;
        }

        @Override
        public void run() {

            Scanner scann = new Scanner(System.in);
            String word = scann.nextLine();
            System.out.println(word);
            if (word != null)
                System.out.println(mathTread.getFugu());
        }


    }

    public static class TreadMatch implements Runnable {
        private int fugu = 0;

        @Override
        public void run() {
            for (; ; ) {
                fugu += 1;
            }

        }

        public int getFugu() {
            return fugu;
        }
    }
}


