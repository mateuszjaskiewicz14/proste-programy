package Calk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window {
    private JFrame window = new JFrame("Caclculator");
    private JTextField imputnumber1 = new JTextField();
    private JTextField imputnumber2 = new JTextField();
    private JButton buttonAddition = new JButton("+");
    private JButton buttonSubtracion = new JButton("-");
    private JLabel resoult = new JLabel("Resoult :");

    public Window() {
        window.setLayout(null);
        layaut();
        addFunctton();
        function();
        setViue();
    }

    public void layaut() {
        window.setBounds(600, 400, 600, 450);
        imputnumber1.setBounds(0, 0, 600, 100);
        imputnumber2.setBounds(0, 100, 600, 100);
        buttonAddition.setBounds(0, 200, 300, 100);
        buttonSubtracion.setBounds(300, 200, 300, 100);
        resoult.setBounds(0, 300, 600, 100);
    }

    public void addFunctton() {
        window.add(imputnumber1);
        window.add(imputnumber2);
        window.add(buttonAddition);
        window.add(buttonSubtracion);
        window.add(resoult);

    }

    public double conwers(JTextField a) {
        double number11 = testImput(a);
        return number11;
    }

    public void function() {


        buttonSubtracion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double x = conwers(imputnumber1) - conwers(imputnumber2);
                resoult.setText("" + x);

            }
        });
        buttonAddition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double x = conwers(imputnumber1) + conwers(imputnumber2);
                resoult.setText("" + x);
            }
        });
    }

    public double testImput(JTextField imput) {
        String imput1 = imput.getText();
        try {
            imput.setBackground(Color.WHITE);
            return Double.parseDouble(imput1);

        } catch (Exception e) {
            imput.setBackground(Color.RED);
            resoult.setText("invalid imput");
            return Double.MIN_VALUE;
        }

    }

    public void setViue() {
        window.setVisible(false);
    }

}


