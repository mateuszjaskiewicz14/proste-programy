package pass;

import sun.print.ServiceDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WrongPassword{
    JDialog dialogWrong = new JDialog();
    JButton buttonWrong = new JButton();
    JLabel labelWrong = new JLabel("WRONG");
    public WrongPassword(){
        dialogWrong.setBounds(50,50,500,500);
        buttonWrong.setBounds(350,350,75, 25);
        labelWrong.setBounds(250,250,200,200);
        dialogWrong.add(buttonWrong);
        dialogWrong.add(labelWrong);
        buttonWrong.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideVisible();
            }
        });

    }
    public void schowDialog(){
        dialogWrong.setVisible(true);
    }
    public void hideVisible(){
        dialogWrong.setVisible(false);
    }

}
