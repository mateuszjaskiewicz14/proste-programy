package com.repead.commpareitp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        users.add(new User("a","a",129));
        users.add(new User("a","b",200));
        users.add(new User("b","c",170));
        users.add(new User("z","x",192));
        Collections.sort(users, User.BY_HEIGHT);
    }
}
