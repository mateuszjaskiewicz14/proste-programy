package com.repead;

public class ImmutableUser {
private final String name;
private final String surname;


        public ImmutableUser(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public ImmutableUser setName(String name){
            return new ImmutableUser(name,surname);
        }
        public ImmutableUser setSurname(String surname){
            return new ImmutableUser(name,surname);
        }

}
