package com.repead.linktlist;

public class MyLinktList<T> {
    private Node first;
    private Node last;
    private int size=0;

    public boolean isEmpty() {
        return first == null;
    }

    public int returnSize(){
        return size;
    }

    public void add(T t) {
        Node newNode = new Node(t);
        if (first == null) {
            first = newNode;
            last = first;
        } else {
        last.next = newNode;
        last=newNode;
        }
        size++;
    }

        public T get (T t){
            Node current = first;
            while (current != null) {
                if (t.equals(current.value))
                    return current.value;
                current = current.next;
            }
            new RuntimeException("Value"+ t + "is not in list");
            return null;
        }

        class Node {
            T value;
            Node next;
            public Node(T t){this.value = t;}
        }
}