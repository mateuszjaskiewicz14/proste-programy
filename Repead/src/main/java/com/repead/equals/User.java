package com.repead.equals;

public class User {
    private String name;
    private String surname;
    private int age;
    private String id;

    public User(String name, String surname, int age, String id){
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.id=id;
    }

    @Override
    public boolean equals(Object o){
        if (!(o instanceof User))return false;
        User that = (User) o;
        return this.age == that.age
                && this.name.equals(that.name)
                && this.surname.equals(that.surname);
    }

    private int haschCode(){
        return age + name.hashCode() + surname.hashCode();
    }
}
