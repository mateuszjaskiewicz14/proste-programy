package com.repead.generics;


import java.util.Arrays;

public class MayArrayList<I> {
    private static final int INT = 10;
    private int newSize;
    private Object[] tab = new Object[INT];
    private int size = 0;

    public void addValue(I element){
        growTab();
        tab[size] = element;
        size++;
    }
    public int getSize(){
        return size;
    }

    public void growTab (){
        if(tab.length==size){
            tab = Arrays.copyOf(tab,tab.length*INT);
        }
    }

    public I get(int i){
        if(i > size || i < 0)
            throw new RuntimeException("Out of bound");
        return (I) tab[i];
    }

    public void remuveValueByIndex(int i){

    }

}
