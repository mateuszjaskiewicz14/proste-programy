import java.util.ArrayList;

public class Simulation {

    private ArrayList<Rabbit> femaleRabbit = new ArrayList<>();
    private ArrayList<Rabbit> maleRabbit = new ArrayList<>();


    public Simulation(int maleCount, int femaleCount, long limit) {
        int mountch = 1;
        for (int rabbitNumber = 0; rabbitNumber < limit; rabbitNumber=femaleRabbit.size()+maleRabbit.size()) {
            breading();
            addRabbit(femaleCount,maleCount);
            ageAdder();
            remuveRbbit();
            mountch++;
        }
        System.out.println(mountch);
    }

    private void addRabbit(int female, int male) {
        Rabbit children = new Rabbit();
        if (femaleRabbit.isEmpty() && maleRabbit.isEmpty()){
            for (int j = 0; j < female; j++) {
                femaleRabbit.add(children);
            }
            for (int k = 0; k < male; k++) {
                maleRabbit.add(children);
            }
        }else{
            int breadCounter = breading();
            for (int i = 0; i < breadCounter; i++) {
                for (int j = 0; j < 14; j++) {
                    femaleRabbit.add(children);
                }
                for (int k = 0; k < 5; k++) {
                    maleRabbit.add(children);
                }
            }
        }
    }

    private int breading() {
            int counter = 0;
        if(!femaleRabbit.isEmpty()) {
            for (int i = 0; i < femaleRabbit.size(); i++) {
                if (femaleRabbit.get(counter).inFeritale()) {
                    counter++;
                }
            }
        }
        return counter;
    }

    private void remuveRbbit() {
        for (int i = 0; i < femaleRabbit.size(); i++) {
            if (femaleRabbit.get(i).getAge() == 96) {
                femaleRabbit.remove(i);
            } else ;
        }
        for (int i = 0; i < maleRabbit.size(); i++) {
            if(maleRabbit.get(i).getAge() == 96){
                maleRabbit.remove(i);
            }
        }
    }

    public void ageAdder(){
        for (int i = 0; i < femaleRabbit.size(); i++) {
            Rabbit femaleBunny  = femaleRabbit.get(i);
            femaleRabbit.remove(i);
            femaleBunny.age();
            femaleRabbit.add(i,femaleBunny);
        }
        for (int i = 0; i < maleRabbit.size(); i++) {
            Rabbit maleBunny  = maleRabbit.get(i);
            maleRabbit.remove(i);
            maleBunny.age();
            maleRabbit.add(i,maleBunny);


        }
    }
}
